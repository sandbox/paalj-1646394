<?php 

/**
 * The theme file for quiz dragdrop.
 *
 * Sponsored by: Senter for IKT i utdanningen 
 * Code: paalj
 *
 * @file
 * Theming functions for the dragdrop question type.
 */

/**
 * Theming function for the results
 * 
 * @param array $vars
 */
function theme_quiz_dragdrop_response($vars) {
  static $css_added;
  if (!$css_added) {
    drupal_add_css(drupal_get_path('module', 'quiz_dragdrop') . '/theme/quiz_dragdrop.css');
    $css_added = TRUE;
  }
  
  $rows = array();
  foreach ($vars['result'] as $title => $status) {    
    $rows[] = array(
      array(
        'data' => '<span class="quiz-dragdrop-icon '.AnswerStatus::getCssClass($status).'" title="' . AnswerStatus::getTitle($status) . '"></span>',
        'class' => 'selector-td quiz-dragdrop-icon-cell',
      ),
      $title,
    );
  }
  
  return theme('table', array('header' => NULL, 'rows' => $rows));;
}
